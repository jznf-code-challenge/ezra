import argparse

import yaml

from .pipeline import Pipeline
from .pipeline_executor import PipelineExecutor


class EzraCliError(Exception):
    pass


parser = argparse.ArgumentParser(description="pipeline executor CLI")
parser.add_argument("--file", type=str, help="path to pipeline yaml file")
parser.add_argument("--inputs", nargs="+", type=str, help="inputs")
parser.add_argument("--dry-run", type=bool, default=False, help="dry run")
parser.add_argument("--loglevel", type=str, help="DEBUG, INFO, WARNING, ERROR")
args = parser.parse_args()


def run(file, inputs, dry_run):

    with open(file, "r") as pipeline_file:
        pipeline = yaml.load(pipeline_file, Loader=yaml.FullLoader)

    inputs_dict = {}

    for input_string in inputs:
        input_kv = [i.strip() for i in input_string.split("=")]
        if len(input_kv) == 1:
            inputs_dict.update({input_kv[0]: True})
        elif len(input_kv) == 2:
            inputs_dict.update({input_kv[0]: input_kv[1]})
        else:
            raise EzraCliError(f"Pipeline input {input_string} is ambiguous")

    pipeline = Pipeline(pipeline, input_kwargs=inputs_dict)
    executor = PipelineExecutor(pipeline=pipeline)
    executor.run(dry_run=dry_run)


if __name__ == "__main__":
    run(file=args.file, inputs=args.inputs, dry_run=args.dry_run)
