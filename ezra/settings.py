from pathlib import Path
import os

BASE_DIR = Path(__file__).resolve().parent

CASSETTES_DIR = os.path.join(BASE_DIR, "tests/data/cassettes")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "{levelname} {asctime} {module} {process:d} {thread:d} {message}",
            "style": "{",
        },
        "simple": {
            "format": "{levelname}\t{name}\t\t{message}",
            "style": "{",
        },
    },
    "handlers": {
        "console": {"class": "logging.StreamHandler", "formatter": "simple"},
    },
    "root": {
        "handlers": ["console"],
        "level": "DEBUG",
    },
}
