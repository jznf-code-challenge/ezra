import logging

import networkx as nx
import matplotlib.pyplot as plt
from networkx.drawing.nx_agraph import graphviz_layout

logger = logging.getLogger(__name__)


class PipelineError(Exception):
    pass


class PipelineComponent:
    def __init__(self, key, value):
        logger.debug(f"initailizing PipelineComponent object {key} with {value}")
        self.name = key
        self.runner = value.get("runner")

        self.inputs = []
        for input_string in value.get("inputs"):
            if not input_string.islower():
                PipelineError("Input should be lowercase so humans and/or machines are not confused.")
            if not 1 <= len((input_tuple := tuple(input_string.split(".")))) <= 2:
                raise PipelineError(
                    "Split input segments can be of length 1 if it is a reference to a Pipeline input or 2 if it is"
                    f" a reference to output of other component. Provided {input_string} has {len(input_tuple)}"
                )
            self.inputs.append(input_tuple)

        self.outputs = []
        for output_string in value.get("outputs"):
            if not output_string.islower() or "." in output_string:
                raise PipelineError(
                    "Output string should adhere to basic naming conventions. It should not contain dots and it should"
                    " be lowercase so humans and/or machines are not confused."
                )
            self.outputs.append(output_string)

        self.dependencies = []
        for i in self.inputs:
            if len(i) > 1:
                self.dependencies.append(i)


class Pipeline:
    """
    We need some reliable object. With builtin validation, and other methods like dependency tree solving etc.
    """

    def _build_graph(self):
        G = nx.DiGraph()
        for component in self.components:
            G.add_node(component.name, component=component)
            for dep in [d[0] for d in component.dependencies]:
                G.add_edge(dep, component.name)
        if nx.is_directed_acyclic_graph(G):
            return G
        else:
            raise PipelineError("There seems to be a cycle in the graph.")

    def get_plot(self):
        """
        install pygraphviz so layout works, https://pygraphviz.github.io/documentation/stable/install.html
        otherwise you get ugly random positioning of nodes, which is hard to read if the graph gets more complex.
        :return: matplotlib plot
        """
        try:
            pos = graphviz_layout(self.graph, prog="dot")
        except Exception as e:
            logger.error(f"Install pygraphviz so layout works. {e}")
            pos = None
        nx.draw(self.graph, with_labels=True, pos=pos)
        return plt

    def solve(self):
        """
        :return: ordered node (name, class) tuples
        """
        nodes = []
        for node in nx.topological_sort(self.graph):
            try:
                nodes.append((node, self.graph.nodes[node]["component"]))
            except KeyError as e:
                raise PipelineError(f"The component for dependency named {node} does not exist") from e
        return nodes

    def __init__(self, pipeline_dict, input_kwargs):
        logger.debug(f"initializing Pipeline object with {pipeline_dict}")
        pipeline_spec = pipeline_dict.get("pipeline")
        if not pipeline_spec:
            raise PipelineError('no "pipeline" key specified')

        self.name = pipeline_spec.pop("name", None)
        if not self.name or not isinstance(self.name, str):
            raise PipelineError()

        self.inputs = pipeline_spec.pop("inputs", None)
        self.outputs = pipeline_spec.pop("outputs", None)

        self.input_kwargs = input_kwargs

        self.components = []
        for k, v in pipeline_spec["components"].items():
            self.components.append(PipelineComponent(k, v))

        self.graph = self._build_graph()
