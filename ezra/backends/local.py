import logging
import uuid
import time

from .base import RunnerBaseClass

logger = logging.getLogger(__name__)


class RunnerError(Exception):
    pass


class OCRModel(RunnerBaseClass):
    def run(self, dry_run=False):
        if "page_id" not in self.inputs:
            raise RunnerError("No page_id input provided")
        # Do some work here
        logger.debug(f"Running OCRModel with {self.inputs}")
        if not dry_run:
            logger.debug("This is not dry run, so it takes incredibly long time to complete")
            # Do some work here
            time.sleep(1)
        else:
            logger.debug("This is dry run, so it's quick")
        self.result = {"page_id": str(uuid.uuid4())}
        logger.debug(f"the result is {self.result}")
        return self.result


class ImagePreprocessor(RunnerBaseClass):
    def run(self, dry_run=False):
        if "document_id" not in self.inputs or "page_num" not in self.inputs:
            raise RunnerError("No document_id or page_num inputs provided")
        logger.debug(f"Running ImagePreprocessor with {self.inputs}")
        if not dry_run:
            logger.debug("This is not dry run, so it takes incredibly long time to complete")
            # Do some work here
            time.sleep(1)
        else:
            logger.debug("This is dry run, so it's quick")
        self.result = {"page_id": str(uuid.uuid4())}
        logger.debug(f"the result is {self.result}")
        return self.result


class ExtractionModel(RunnerBaseClass):
    def run(self, dry_run=False):
        if "page_id" not in self.inputs:
            raise RunnerError("No page_id input provided")
        logger.debug(f"Running ExtractionModel with {self.inputs}")
        if not dry_run:
            logger.debug("This is not dry run, so it takes incredibly long time to complete")
            # Do some work here
            time.sleep(1)
        else:
            logger.debug("This is dry run, so it's quick")
        self.result = {"extractions": str(uuid.uuid4())}
        logger.debug(f"the result is {self.result}")
        return self.result


class MashupModel(RunnerBaseClass):
    def run(self, dry_run=False):
        logger.debug("This model takes in everything you put in and doesn't care much")
        logger.debug(f"Running ExtractionModel with {self.inputs}")
        if not dry_run:
            logger.debug("This is not dry run, so it takes incredibly long time to complete")
            # Do some work here
            time.sleep(1)
        else:
            logger.debug("This is dry run, so it's quick")
        self.result = {"extractions": str(uuid.uuid4())}
        logger.debug(f"the result is {self.result}")
        return self.result
