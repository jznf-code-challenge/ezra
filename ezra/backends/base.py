from abc import ABC, abstractmethod


class RunnerBaseClass(ABC):
    """
    We need to be sure certain things are implemented in the runner classes so we use ABC to enforce them
    and check if runner classes are inheriting from this RunnerBaseClass when we import runner classes later.
    We IGNORE runner classes which don't inherit from RunnerBaseClass.
    """

    def __init__(self, inputs, outputs):
        self.inputs = inputs
        self.outputs = outputs
        self.result = None

    @abstractmethod
    def run(self, dry_run=False) -> dict:
        raise NotImplementedError
