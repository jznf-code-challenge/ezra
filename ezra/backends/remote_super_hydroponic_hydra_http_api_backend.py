import logging
import requests

from .base import RunnerBaseClass

logger = logging.getLogger(__name__)


class SuperHydroponicHydraException(RuntimeError):
    pass


def _get_safe_reponse_data():
    response = requests.get("https://httpbin.org/uuid")
    if response.status_code == 200:
        try:
            return response.json()
        except AttributeError as e:
            raise SuperHydroponicHydraException("The upstream API did not return any usable data") from e
    else:
        raise SuperHydroponicHydraException(f"The upstream API returned {response.status_code}")


class OCRModel(RunnerBaseClass):
    def run(self, dry_run=False):
        # We call external service here
        response_data = _get_safe_reponse_data()
        self.result = {"page_id": response_data.get("uuid")}
        logger.debug(f"the result is {self.result}")
        return self.result


class ImagePreprocessor(RunnerBaseClass):
    def run(self, dry_run=False):
        # We call external service here
        response_data = _get_safe_reponse_data()
        self.result = {"page_id": response_data.get("uuid")}
        logger.debug(f"the result guid is {self.result}")
        return self.result


class ExtractionModel(RunnerBaseClass):
    def run(self, dry_run=False):
        # We call external service here
        response_data = _get_safe_reponse_data()
        self.result = {"extractions": response_data.get("uuid")}
        logger.debug(f"the result is {self.result}")
        return self.result
