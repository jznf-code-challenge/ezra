from unittest.case import TestCase
import os
import uuid

import yaml
import vcr

from ezra import settings
from ezra.pipeline import Pipeline
from ezra.pipeline_executor import PipelineExecutor
from ezra.backends.local import RunnerError


def get_pipeline_dict(file_path):
    with open(file_path, "r") as fileobj:
        return yaml.load(fileobj, Loader=yaml.FullLoader)


class BasicPipelineTest(TestCase):
    def test_pipeline_branching_merge_branch_merge(self):
        pipeline_dict = get_pipeline_dict(os.path.join(settings.BASE_DIR, "tests/data/pipeline_merge_branch_merge.yml"))
        pipeline = Pipeline(pipeline_dict, input_kwargs={"document_id": "D0", "page_num": 0})
        output = PipelineExecutor(pipeline=pipeline).run(backend="local")
        self.assertIsInstance(uuid.UUID(output["image_preprocessing"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["image_ocr_1"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["image_ocr_2"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["extractor_1"]["extractions"]), uuid.UUID)

    def test_pipeline_basic_ok(self):
        pipeline_dict = get_pipeline_dict(os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml"))
        pipeline = Pipeline(pipeline_dict, input_kwargs={"document_id": "D0", "page_num": 0})
        output = PipelineExecutor(pipeline=pipeline).run(backend="local")
        self.assertIsInstance(uuid.UUID(output["image_preprocessing"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["image_ocr"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["extractor"]["extractions"]), uuid.UUID)

    def test_pipeline_basic_ko(self):
        pipeline_dict = get_pipeline_dict(os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml"))
        pipeline = Pipeline(pipeline_dict, input_kwargs={"nonsense": "D0", "utter_nonsense": 0})
        with self.assertRaises(RunnerError):
            PipelineExecutor(pipeline=pipeline).run(backend="local")

    def test_pipeline_branching(self):
        pipeline_dict = get_pipeline_dict(os.path.join(settings.BASE_DIR, "tests/data/pipeline_branch.yml"))
        pipeline = Pipeline(pipeline_dict, input_kwargs={"document_id": "D0", "page_num": 0})
        output = PipelineExecutor(pipeline=pipeline).run(backend="local")
        self.assertIsInstance(uuid.UUID(output["image_preprocessing"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["image_ocr_1"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["extractor_1"]["extractions"]), uuid.UUID)

    @vcr.use_cassette(os.path.join(settings.CASSETTES_DIR, "test_pipeline_remote"))
    def test_pipeline_remote(self):
        pipeline_dict = get_pipeline_dict(os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml"))
        pipeline = Pipeline(pipeline_dict, input_kwargs={"document_id": "D0", "page_num": 0})
        output = PipelineExecutor(pipeline=pipeline).run(backend="remote_super_hydroponic_hydra_http_api_backend")
        self.assertIsInstance(uuid.UUID(output["image_preprocessing"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["image_ocr"]["page_id"]), uuid.UUID)
        self.assertIsInstance(uuid.UUID(output["extractor"]["extractions"]), uuid.UUID)


class RunTest(TestCase):
    def test_entrypoint_ok(self):
        exit_status = os.system(
            f'python -m ezra.cli --file {os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml")} '
            f"--inputs document_id=1 page_num=1"
        )
        self.assertEqual(exit_status, 0)

    def test_entrypoint_dry_run_ok(self):
        exit_status = os.system(
            f'python -m ezra.cli --file {os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml")} '
            f"--inputs document_id=1 page_num=1 --dry-run true"
        )
        self.assertEqual(exit_status, 0)

    def test_entrypoint_ko(self):
        exit_status = os.system(
            f'python -m ezra.cli --file {os.path.join(settings.BASE_DIR, "tests/data/pipeline_basic.yml")} '
            f"--inputs input0=0 input2=input2"
        )
        self.assertEqual(exit_status, 256)
