import logging.config
import importlib
import inspect
import sys

from .pipeline import Pipeline
from .backends.base import RunnerBaseClass
from . import settings

logging.config.dictConfig(settings.LOGGING)
logger = logging.getLogger(__name__)


class PipelineExecutorError(Exception):
    pass


class PipelineExecutor:
    @staticmethod
    def _get_runners():
        path = settings.BASE_DIR
        module_files = list(path.glob("backends/**/*.py"))
        modules = [module for module in module_files if not module.name.startswith("_") and module.name != "base.py"]
        runners = {}
        for module in modules:
            module_name = module.name[:-3]
            import_path = f"ezra.backends.{module_name}"
            importlib.import_module(import_path)
            classes = inspect.getmembers(sys.modules[import_path], inspect.isclass)
            classes = {k: v for k, v in classes if v.__module__ == import_path and issubclass(v, RunnerBaseClass)}
            runners.update({module_name: classes})
        return runners

    def __init__(self, pipeline):
        if not isinstance(pipeline, Pipeline):
            raise PipelineExecutorError("Please ensure it is 'ezra.pipeline.Pipeline' instance.")
        self.pipeline = pipeline
        self.runners = self._get_runners()
        logger.debug(f"PipelineExecutor instance is constructed {self.pipeline}")

    def run(self, backend="local", dry_run=False):
        """
        :param inputs:
        :param backend: we can select where we want to actually run the pipeline, default is local
        :return: results
        """

        logger.info(
            f"Running pipeline: {self.pipeline} with inputs "
            f"params: {self.pipeline.input_kwargs} with backend: {backend}"
        )
        results = {}

        for _, component in self.pipeline.solve():
            logger.info(f"Doing component '{component.name}'")
            try:
                runner_class = self.runners[backend][component.runner]
            except KeyError as e:
                raise PipelineExecutorError(f"There's no '{component.runner}' runner in '{backend}' backend") from e

            runner_inputs = {}

            for dependency_component_name, input_key in component.dependencies:
                try:
                    input_value = results[dependency_component_name][input_key]
                    runner_inputs.update({input_key: input_value})
                except KeyError as e:
                    raise PipelineExecutorError(
                        f"The dependency does not exist: {dependency_component_name, input_key}"
                    ) from e

            for arg in [i for i in component.inputs if len(i) == 1 and i[0] in self.pipeline.input_kwargs.keys()]:
                if arg[0] in runner_inputs.keys():
                    raise PipelineExecutorError("The key is already in input dict, this should't happen at all")
                runner_inputs.update({arg[0]: self.pipeline.input_kwargs[arg[0]]})

            runner_instance = runner_class(inputs=runner_inputs, outputs=self.pipeline.outputs)
            runner_results = runner_instance.run(dry_run=dry_run)

            results.update({component.name: runner_results})

        logger.debug(f"the results are: {results}")
        return results
