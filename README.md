# ezra

*This is an archived years old code challenge response.* **It' nothing like a production code**, it is meant for educational purposes only.

It's called ezra. Random short, non-frequent name.

## An Example Pipeline Executor

Summary:

The challenge is to create an executor for computational graphs defined in a YAML file, including a pipeline with properties like name, inputs, outputs, and components. Each component has a name, input/output parameters, and an executable reference. Dependencies are determined based on input-output relationships. Unique naming is required for each component's parameters. The CLI tool needs the YAML file and input values, if any. It simulates component execution in a dependency order, with components either passing through input values or generating random outputs, and logs execution details.

## Install

For local development, you can use:

`pip install --force-reinstall --no-deps -e ".[dev]"`

## Run

it can be used as a library by importing `PipelineExecutor` and `Pipeline` classes or as a standalone tool like:

`python -m ezra.cli --file pipeline.yml --inputs input0=0 input2=2 --dry-run True`

## Notes

- see `ezra/tests/data` for various pipelines including merging and branching
