from setuptools import setup, find_packages

setup(
    name="ezra",
    url="https://gitlab.com/jznf/rossum_homework/ezra",
    author="Jan Zavesky",
    author_email="jan@zavesky.info",
    packages=find_packages(),
    install_requires=["pyyaml==5.4.1", "requests==2.25.1", "networkx==2.5"],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pylint==2.7.2",
            "pygraphviz==1.7",
            "matplotlib==3.4.0",
            "vcrpy==4.1.1",
            "factory-boy==3.2.0",
        ]
    },
    zip_safe=False,
    include_package_data=True,
    package_data={"": ["*.hcl", "*.json", "*.crt", "py.typed"]},
    version="0.0.2",
    description="A pipeline executor - a homework assignment for Rossum.ai",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
    ],
)
